﻿using UnityEngine;
using MattRGeorge.DayNightCycle.Engines;

namespace Assets.Drivers
{
    public class DateEngineDriver : MonoBehaviour
    {
        [SerializeField] private TimeEngine timeEngine = null;
        [SerializeField] private DateEngine dateEngine = null;

        private DNCDatestamp startDate = null;

        #region Logging
        private void OnDaysComplete(int daysPassed)
        {
            DNCDatestamp date = dateEngine.NowDatestamp;
            Debug.Log("Date (" + dateEngine.GetSeasonInformation(date.season).name + "): " + dateEngine.GetMonthInformation(date.date.month).name + " " + (date.date.day + 1) + ", " + date.date.year);
        }

        private void UpdateAge(int daysPassed)
        {
            DNCAge age = dateEngine.FindAge(startDate, dateEngine.NowDatestamp);
            Debug.Log(string.Format("Game age: {0} day(s), {1} month(s), {2} year(s), {3} total day(s)", age.days, age.months, age.years, age.totalDays));


            age = dateEngine.FindAge(dateEngine.WorldStart, dateEngine.NowDatestamp);
            Debug.Log(string.Format("World age: {0} day(s), {1} month(s), {2} year(s), {3} total day(s)", age.days, age.months, age.years, age.totalDays));
        }
        #endregion

        #region Tests
        private void CheckLeapYearTest()
        {
            Debug.Assert(!dateEngine.CheckLeapYear(1993), "1993 is not a leap year!");
            Debug.Assert(dateEngine.CheckLeapYear(1996), "1996 is a leap year!");
        }

        private void CheckSeasonTest()
        {
            int season = dateEngine.CheckSeason(2, 19);
            Debug.Assert(season == 3, string.Format("Season does not match! {0} != 3 (Winter)", season));
            season = dateEngine.CheckSeason(2, 20);
            Debug.Assert(season == 0, string.Format("Season does not match! {0} != 0 (Spring)", season));

            season = dateEngine.CheckSeason(5, 20);
            Debug.Assert(season == 0, string.Format("Season does not match! {0} != 0 (Spring)", season));
            season = dateEngine.CheckSeason(5, 21);
            Debug.Assert(season == 1, string.Format("Season does not match! {0} != 1 (Summer)", season));

            season = dateEngine.CheckSeason(8, 21);
            Debug.Assert(season == 1, string.Format("Season does not match! {0} != 1 (Summer)", season));
            season = dateEngine.CheckSeason(8, 22);
            Debug.Assert(season == 2, string.Format("Season does not match! {0} != 2 (Fall)", season));

            season = dateEngine.CheckSeason(11, 20);
            Debug.Assert(season == 2, string.Format("Season does not match! {0} != 2 (Fall)", season));
            season = dateEngine.CheckSeason(1, 21);
            Debug.Assert(season == 3, string.Format("Season does not match! {0} != 3 (Winter)", season));
        }

        private void GetMonthDaysTest()
        {
            int days = dateEngine.GetTotalMonthDays(0, 1993);
            Debug.Assert(days == 31, string.Format("Days doesn't match! {0} != 31", days));

            days = dateEngine.GetTotalMonthDays(1, 1996);
            Debug.Assert(days == 29, string.Format("Days doesn't match! {0} != 29", days));
            days = dateEngine.GetTotalMonthDays(1, 1993);
            Debug.Assert(days == 28, string.Format("Days doesn't match! {0} != 28", days));

            days = dateEngine.GetTotalMonthDays(2, 1993);
            Debug.Assert(days == 31, string.Format("Days doesn't match! {0} != 31", days));

            days = dateEngine.GetTotalMonthDays(3, 1993);
            Debug.Assert(days == 30, string.Format("Days doesn't match! {0} != 30", days));

            days = dateEngine.GetTotalMonthDays(4, 1993);
            Debug.Assert(days == 31, string.Format("Days doesn't match! {0} != 31", days));

            days = dateEngine.GetTotalMonthDays(5, 1993);
            Debug.Assert(days == 30, string.Format("Days doesn't match! {0} != 30", days));

            days = dateEngine.GetTotalMonthDays(6, 1993);
            Debug.Assert(days == 31, string.Format("Days doesn't match! {0} != 31", days));

            days = dateEngine.GetTotalMonthDays(7, 1993);
            Debug.Assert(days == 31, string.Format("Days doesn't match! {0} != 31", days));

            days = dateEngine.GetTotalMonthDays(8, 1993);
            Debug.Assert(days == 30, string.Format("Days doesn't match! {0} != 30", days));

            days = dateEngine.GetTotalMonthDays(9, 1993);
            Debug.Assert(days == 31, string.Format("Days doesn't match! {0} != 31", days));

            days = dateEngine.GetTotalMonthDays(10, 1993);
            Debug.Assert(days == 30, string.Format("Days doesn't match! {0} != 30", days));

            days = dateEngine.GetTotalMonthDays(11, 1993);
            Debug.Assert(days == 31, string.Format("Days doesn't match! {0} != 31", days));
        }

        private void FindAgeHelper(DNCAge result, int years, int months, int days, ulong totalDays)
        {
            Debug.Assert(result != null, "Result should not be null!");
            if(result != null)
            {
                Debug.Assert(result.years == years, string.Format("Years don't match! {0} != {1}", result.years, years));
                Debug.Assert(result.months == months, string.Format("Months don't match! {0} != {1}", result.months, months));
                Debug.Assert(result.days == days, string.Format("Days don't match! {0} != {1}", result.days, days));
                Debug.Assert(result.totalDays == totalDays, string.Format("Total days don't match! {0} != {1}", result.totalDays, totalDays));
            }
        }
        private void FindAgeTest()
        {
            DNCAge age = dateEngine.FindAge(null, null);
            FindAgeHelper(age, 0, 0, 0, 0);
            age = dateEngine.FindAge(new DNCDatestamp(10, 0, 1993, 0, false, 9817), new DNCDatestamp(28, 10, 2019, 0, false, 0));
            FindAgeHelper(age, 0, 0, 0, 0);
            age = dateEngine.FindAge(new DNCDatestamp(10, 0, 1993, 0, false, 0), new DNCDatestamp(28, 10, 2019, 0, false, 0));
            FindAgeHelper(age, 0, 0, 0, 0);

            age = dateEngine.FindAge(new DNCDatestamp(10, 0, 1993, 0, false, 0), new DNCDatestamp(28, 10, 2019, 0, false, 9817));
            FindAgeHelper(age, 26, 10, 17, 9817);
        }

        private void FindDateHelper(DNCDatestamp result, int month, int day, int year, ulong totalDays)
        {
            Debug.Assert(result != null, "FindFutureDate() should not return null!");
            if (result != null)
            {
                Debug.Assert(result.date.day == day - 1, string.Format("Day does not match: {0} != {1}", result.date.day, day - 1));
                Debug.Assert(result.date.month == month - 1, string.Format("Month does not match: {0} != {1}", result.date.month, month - 1));
                Debug.Assert(result.date.year == year, string.Format("Year does not match: {0} != {1}", result.date.year, year));
                Debug.Assert(result.totalDays == totalDays, string.Format("Total days does not match: {0} != {1}", result.totalDays, totalDays));
            }
        }
        private void FindDateTest()
        {
            DNCDatestamp start = new DNCDatestamp(10, 0, 1993, 0, false, 10000);

            DNCDatestamp end = dateEngine.FindDate(start, 0);
            FindDateHelper(end, end.date.month + 1, end.date.day + 1, end.date.year, end.totalDays);
            end = dateEngine.FindDate(start, -11000);
            FindDateHelper(end, end.date.month + 1, end.date.day + 1, end.date.year, end.totalDays);

            end = dateEngine.FindDate(start, 1000);
            FindDateHelper(end, 10, 8, 1995, 11000);

            end = dateEngine.FindDate(start, -100);
            FindDateHelper(end, 6, 30, 1995, 10900);
            end = dateEngine.FindDate(start, -500);
            FindDateHelper(end, 2, 15, 1994, 10400);
            end = dateEngine.FindDate(start, -700);
            FindDateHelper(end, 3, 17, 1992, 9700);
            end = dateEngine.FindDate(start, -1000);
            FindDateHelper(end, 6, 21, 1989, 8700);
            end = dateEngine.FindDate(start, -8000);
            FindDateHelper(end, 7, 27, 1967, 700);

            end = dateEngine.FindDate(start, 8000);
            FindDateHelper(end, 6, 21, 1989, 8700);
            end = dateEngine.FindDate(start, 1000);
            FindDateHelper(end, 3, 17, 1992, 9700);
            end = dateEngine.FindDate(start, 1300);
            FindDateHelper(end, 10, 8, 1995, 11000);
        }

        private void IsFutureTest()
        {
            Debug.Assert(!dateEngine.IsFuture(null, null), "Should return false if null inputs!");
            Debug.Assert(!dateEngine.IsFuture(new DNCDate(30, 11, 1993), null), "Should return false if null inputs!");
            Debug.Assert(!dateEngine.IsFuture(null, new DNCDate(30, 11, 1993)), "Should return false if null inputs!");

            Debug.Assert(dateEngine.IsFuture(new DNCDate(30, 11, 1993), new DNCDate(0, 0, 1994)), "1/1/1994 should be in the future from 12/31/1993!");
            Debug.Assert(dateEngine.IsFuture(new DNCDate(10, 0, 1993), new DNCDate(10, 27, 2019)), "11/28/2019 should be in the future from 1/11/1993!");
            Debug.Assert(dateEngine.IsFuture(new DNCDate(9, 0, 1994), new DNCDate(9, 1, 1994)), "2/10/1994 should be in the future from 1/10/1994!");
            Debug.Assert(dateEngine.IsFuture(new DNCDate(9, 0, 2019), new DNCDate(0, 10, 2019)), "1/11/2019 should be in the future from 1/10/2019!");

            Debug.Assert(!dateEngine.IsFuture(new DNCDate(0, 0, 1994), new DNCDate(30, 11, 1993)), "12/31/1993 should not be in the future from 1/1/1994!");
            Debug.Assert(!dateEngine.IsFuture(new DNCDate(10, 27, 2019), new DNCDate(10, 0, 1993)), "1/11/1993 should not be in the future from 11/28/2019!");
            Debug.Assert(!dateEngine.IsFuture(new DNCDate(9, 1, 1994), new DNCDate(9, 0, 1994)), "1/10/1994 should not be in the future from 2/10/1994!");
            Debug.Assert(!dateEngine.IsFuture(new DNCDate(0, 10, 2019), new DNCDate(9, 0, 2019)), "1/10/2019 should not be in the future from 1/11/2019!");

            Debug.Assert(!dateEngine.IsFuture(new DNCDate(0, 0, 1994), new DNCDate(0, 0, 1994)), "1/1/1994 should not be in the future from 1/1/1994!");
        }

        private void RunTests()
        {
            CheckLeapYearTest();
            CheckSeasonTest();
            GetMonthDaysTest();
            FindAgeTest();
            FindDateTest();
            IsFutureTest();
        }
        #endregion

        #region Unity Methods
        private void Awake()
        {
            if(timeEngine != null)
            {
                timeEngine.OnDaysComplete += OnDaysComplete;
                timeEngine.OnDaysComplete += UpdateAge;
            }

            RunTests();
        }
        private void Start()
        {
            startDate = dateEngine.NowDatestamp;
            OnDaysComplete(0);
        }

        private void Update()
        {
            if (Input.GetKeyUp(KeyCode.M)) timeEngine?.AddTime(0, 0, 0, 365);
            if (Input.GetKeyUp(KeyCode.N)) timeEngine?.AddTime(0, 0, 0, 31);
            if (Input.GetKeyUp(KeyCode.B)) timeEngine?.AddTime(0, 0, 0, 1);
        }
        #endregion
    }
}
