﻿using UnityEngine;
using MattRGeorge.DayNightCycle.Engines;

namespace Assets.Drivers
{
    public class TimeEngineDriver : MonoBehaviour
    {
        [SerializeField] private TimeEngine timeEngine = null;
        string debugTxt = "";

        private void TimeUpdated(int secsPassed, DNCTimestamp time)
        {
            if (timeEngine != null)
            {
                debugTxt = "";
                debugTxt += "Time: " + time.time.hr + ":" + time.time.min + ":" + time.time.sec + " " + time.totalSecs + ":" + time.totalTimeUpdates + " " + secsPassed;
                debugTxt += "\nDay Progress: " + time.dayProgress;

                Debug.Log(debugTxt);
            }
        }
        private void DaysPassed(int daysPassed)
        {
            Debug.Log(daysPassed + " day(s) passed.");
        }

        private void Awake()
        {
            if (timeEngine != null)
            {
                timeEngine.OnTimeUpdate += TimeUpdated;
                timeEngine.OnDaysComplete += DaysPassed;
            }
        }
        private void Start()
        {
            //timeEngine.StartTime();
        }

        private void Update()
        {
            if (Input.GetKeyUp(KeyCode.P)) timeEngine?.Pause();
            if (Input.GetKeyUp(KeyCode.O)) timeEngine?.Resume();
            if (Input.GetKeyUp(KeyCode.J)) timeEngine?.AddTime(35, 0, 0);
            if (Input.GetKeyUp(KeyCode.K)) timeEngine?.AddTime(0, 35, 0);
            if (Input.GetKeyUp(KeyCode.L)) timeEngine?.AddTime(0, 0, 13);
        }
    }
}
