﻿using System;
using UnityEngine;
using MattRGeorge.DayNightCycle.Engines;

namespace MattRGeorge.DayNightCycle
{
    /// <summary>
    /// The main access point of the day night cycle system.
    /// This class has its own time and date engines that other objects are able to access to update what they need to.
    /// </summary>
    [RequireComponent(typeof(TimeEngine))]
    [RequireComponent(typeof(DateEngine))]
    public class DayNightCycle : MonoBehaviour
    {
        #region Variables
        public static DayNightCycle SINGLETON = null;

        [Tooltip("Setup a singleton?")]
        [SerializeField] private bool useSingleton = true;

        /// <summary>
        /// The time engine that this day night cycle is using.
        /// </summary>
        public TimeEngine TimeEngineObj
        {
            get
            {
                return timeEngine;
            }
        }
        /// <summary>
        /// The date engine that this day night cycle is using.
        /// </summary>
        public DateEngine DateEngineObj
        {
            get
            {
                return dateEngine;
            }
        }

        private TimeEngine timeEngine = null;
        private DateEngine dateEngine = null;
        #endregion

        #region Methods
        /// <summary>
        /// Initial setup.
        /// </summary>
        private void Setup()
        {
            if (useSingleton)
            {
                if (DayNightCycle.SINGLETON == null) SINGLETON = this;
                else
                {
                    Debug.LogError("More than one 'DayNightCycle' singletons detected!");
                    Destroy(this);
                }
            }

            timeEngine = GetComponent<TimeEngine>();
            dateEngine = GetComponent<DateEngine>();
        }
        /// <summary>
        /// Initital setup after Setup().
        /// </summary>
        private void PostSetup()
        {
            timeEngine.StartTime();
            dateEngine.StartDate();
        }
        #endregion

        #region Unity Methods
        private void Awake()
        {
            Setup();
        }
        private void Start()
        {
            PostSetup();
        }
        #endregion
    }
}
