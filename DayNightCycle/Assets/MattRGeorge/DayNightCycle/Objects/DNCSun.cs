﻿using UnityEngine;
using MattRGeorge.DayNightCycle.Engines;

namespace MattRGeorge.DayNightCycle.Objects
{
    /// <summary>
    /// A simple directional light sun that updates its rotation by the current time of day.
    /// </summary>
    public class DNCSun : MonoBehaviour
    {
        #region Variables
        [Tooltip("The DayNightCycle that this sun uses (will use the DayNightCycle singleton if null).")]
        [SerializeField] private DayNightCycle dayNightCycle = null;
        [Tooltip("The amount of updates to skip between sun rotation updates.")]
        [SerializeField] private float updateFreq = 0;

        private float lastSunUpdate = 0;
        #endregion

        #region Methods
        /// <summary>
        /// Update the rotation of the sun.
        /// </summary>
        /// <param name="secsPassed">The seconds that passed.</param>
        /// <param name="time">The current time.</param>
        private void UpdateSun(int secsPassed, DNCTimestamp time)
        {
            if (Time.time - lastSunUpdate >= updateFreq)
            {
                transform.localEulerAngles = new Vector3(time.dayProgress * 360 - 90, 0, 0);
                lastSunUpdate = Time.time;
            }
        }

        /// <summary>
        /// Initial setup.
        /// </summary>
        private void Setup()
        {
            if (DayNightCycle.SINGLETON == null && dayNightCycle == null) Debug.LogError("No 'DayNightCycle' available or assigned!");
            else
            {
                if (dayNightCycle != null) dayNightCycle.TimeEngineObj.OnTimeUpdate += UpdateSun;
                else DayNightCycle.SINGLETON.TimeEngineObj.OnTimeUpdate += UpdateSun;
            }

            lastSunUpdate = -updateFreq;
        }
        #endregion

        #region Unity Methods
        private void Start()
        {
            Setup();
        }
        #endregion
    }
}
