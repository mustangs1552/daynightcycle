﻿using System;
using UnityEngine;

namespace MattRGeorge.DayNightCycle.Engines
{
    /// <summary>
    /// Manages the days, months, years, and seasonsof in-game date.
    /// </summary>
    public class DateEngine : MonoBehaviour
    {
        #region Variables
        #region Public
        [Tooltip("The time engine that this date engine should use.")]
        [SerializeField] private TimeEngine timeEngine = null;
        [Tooltip("The world's starting date.")]
        [SerializeField] private DNCDate worldStartDate = new DNCDate(10, 0, 1993);
        [Tooltip("The amount of days to go forward for the game's start date.")]
        [SerializeField] private int worldStartDaysForward = 0;
        [Tooltip("The months' information.")]
        [SerializeField] private Month[] months =
        {
            new Month("January", 31),
            new Month("February", 28, 1),
            new Month("March", 31),
            new Month("April", 30),
            new Month("May", 31),
            new Month("June", 30),
            new Month("July", 31),
            new Month("August", 31),
            new Month("September", 30),
            new Month("October", 31),
            new Month("November", 30),
            new Month("December", 31)
        };
        [Tooltip("The seasons' information.")]
        [SerializeField] private Season[] seasons =
        {
            new Season("Spring", 20, 2),
            new Season("Summer", 21, 5),
            new Season("Fall", 22, 8),
            new Season("Winter", 21, 11)
        };

        /// <summary>
        /// The current date.
        /// </summary>
        public DNCDate NowDate
        {
            get
            {
                return new DNCDate(currDay, currMonth, currYear);
            }
        }
        /// <summary>
        /// The current date with season and wether or not its a leap year.
        /// These values can't be changed once set.
        /// </summary>
        public DNCDatestamp NowDatestamp
        {
            get
            {
                return new DNCDatestamp(NowDate, currSeason, CheckLeapYear(), totalDays);
            }
        }
        /// <summary>
        /// The start of the world.
        /// totalDays will equal 0 at this point.
        /// </summary>
        public DNCDatestamp WorldStart
        {
            get
            {
                return worldStart;
            }
        }

        /// <summary>
        /// The date was changed.
        /// Param (DNCDatestamp): The new date.
        /// </summary>
        public Action<DNCDatestamp> OnDateChanged { get; set; }
        /// <summary>
        /// The month changed.
        /// Param (int): the index of the new month.
        /// </summary>
        public Action<int> OnMonthChanged { get; set; }
        /// <summary>
        /// The year changed.
        /// Param (int): The new year.
        /// </summary>
        public Action<int> OnYearChanged { get; set; }
        /// <summary>
        /// The season changed.
        /// Param (int): The index of the new season.
        /// </summary>
        public Action<int> OnSeasonChanged { get; set; }

        /// <summary>
        /// The months of this date engine.
        /// </summary>
        public Month[] Months
        {
            get
            {
                return months;
            }
        }
        /// <summary>
        /// The seasons of this date engine.
        /// </summary>
        public Season[] Seasons
        {
            get
            {
                return seasons;
            }
        }
        #endregion

        #region Private
        private DNCDatestamp worldStart = new DNCDatestamp();
        private int currDay = 0;
        private int currMonth = 0;
        private int currYear = 0;
        private int currSeason = 0;
        private ulong totalDays = 0;
        #endregion
        #endregion

        #region Methods
        #region Public
        /// <summary>
        /// Set the starting date.
        /// </summary>
        public void StartDate()
        {
            currDay = worldStartDate.day;
            if (currMonth != worldStartDate.month)
            {
                currMonth = worldStartDate.month;
                OnMonthChanged?.Invoke(currMonth);
            }
            if (currYear != worldStartDate.year)
            {
                currYear = worldStartDate.year;
                OnYearChanged?.Invoke(currYear);
            }

            currSeason = CheckSeason();
            totalDays = 0;
            worldStart = NowDatestamp;

            if (worldStartDaysForward != 0) timeEngine.AddTime(0, 0, 0, worldStartDaysForward);
            else OnDateChanged?.Invoke(NowDatestamp);
        }

        /// <summary>
        /// Checks the given year to see if it is a leap year. Returns true if it is.
        /// </summary>
        /// <param name="year">The year to check. Uses current year by default.</param>
        public bool CheckLeapYear(int year = -1)
        {
            if (year <= -1) year = currYear;

            if (year > -1 && (year % 4 == 0 && (!(year % 100 == 0) || year % 400 == 0))) return true;

            return false;
        }

        /// <summary>
        /// Checks the current month and day and returns the season they rest in.
        /// </summary>
        public int CheckSeason()
        {
            return CheckSeason(currMonth, currDay);
        }
        /// <summary>
        /// Checks the given month and day and returns the season they rest in.
        /// </summary>
        /// <param name="month">The month's index to check.</param>
        /// <param name="day">The 0 base day to check.</param>
        public int CheckSeason(int month, int day)
        {
            int s = -1;

            // After Spring's start month.
            if (month > seasons[0].startDate.month)
            {
                // After Summer's start month.
                if (month > seasons[1].startDate.month)
                {
                    // After Fall's start month.
                    if (month > seasons[2].startDate.month)
                    {
                        if (month == seasons[3].startDate.month && day >= seasons[3].startDate.day) s = 3;
                        else s = 2;
                    }
                    // Same as Fall's start month.
                    else if (month == seasons[2].startDate.month)
                    {
                        if (day >= seasons[2].startDate.day) s = 2;
                        else s = 1;
                    }
                    // Before Fall's start month.
                    else s = 1;
                }
                // Same as Summer's start month, check day.
                else if (month == seasons[1].startDate.month)
                {
                    if (day >= seasons[1].startDate.day) s = 1;
                    else s = 0;
                }
                // Before Summer's start month.
                else s = 0;
            }
            // Same as Spring's start month, check day.
            else if (month == seasons[0].startDate.month)
            {
                if (day >= seasons[0].startDate.day) s = 0;
                else s = 3;
            }
            // Before Spring's start month.
            else s = 3;

            return s;
        }

        /// <summary>
        /// Get the current season's information.
        /// </summary>
        /// <returns>The current season's information.</returns>
        public Season GetSeasonInformation()
        {
            return GetSeasonInformation(currSeason);
        }
        /// <summary>
        /// Get the information for the given season.
        /// </summary>
        /// <param name="seasonIndex">The index of the desired season.</param>
        /// <returns>The desired season's information.</returns>
        public Season GetSeasonInformation(int seasonIndex)
        {
            if (seasonIndex >= seasons.Length) return new Season();
            else return seasons[seasonIndex];
        }

        /// <summary>
        /// Gets the total days in the current month.
        /// </summary>
        public int GetTotalMonthDays()
        {
            return GetTotalMonthDays(currMonth, currYear);
        }
        /// <summary>
        /// Gets the total days in the given month.
        /// </summary>
        /// <param name="monthIndex">The 0 base month to check.</param>
        public int GetTotalMonthDays(int monthIndex)
        {
            return GetTotalMonthDays(monthIndex, currYear);
        }
        /// <summary>
        /// Gets the total days in the given month and year.
        /// </summary>
        /// <param name="monthIndex">The 0 base month to check.</param>
        /// <param name="year">The year the month is in.</param>
        public int GetTotalMonthDays(int monthIndex, int year)
        {
            Month month = months[monthIndex];

            if (month.leapYearDayAdjustment != 0 && CheckLeapYear(year)) return month.days + month.leapYearDayAdjustment;
            else return month.days;
        }

        /// <summary>
        /// Get the current month's information.
        /// </summary>
        /// <returns>The current month's information.</returns>
        public Month GetMonthInformation()
        {
            return GetMonthInformation(currMonth);
        }
        /// <summary>
        /// Get the information for the given month.
        /// </summary>
        /// <param name="monthIndex">The index of the desired month.</param>
        /// <returns>The desired month's information.</returns>
        public Month GetMonthInformation(int monthIndex)
        {
            if (monthIndex >= months.Length) return new Month();
            else return months[monthIndex];
        }

        /// <summary>
        /// Find the age/difference between the given starting DNCDatestamp and now.
        /// </summary>
        /// <param name="start">The starting point.</param>
        /// <returns>The age/difference.</returns>
        public DNCAge FindAge(DNCDatestamp start)
        {
            return FindAge(start, NowDatestamp);
        }
        /// <summary>
        /// Find the age/difference between two DNCDatestamps.
        /// </summary>
        /// <param name="start">The starting point.</param>
        /// <param name="end">The ending point.</param>
        /// <returns>The age/difference.</returns>
        public DNCAge FindAge(DNCDatestamp start, DNCDatestamp end)
        {
            DNCAge age = new DNCAge();
            if (start == null || end == null || start.totalDays >= end.totalDays) return age;

            age.totalDays = end.totalDays - start.totalDays;
            try
            {
                age.days = (int)age.totalDays;
            }
            catch(InvalidCastException e)
            {
                Debug.LogError("Unable to cast from ulong (" + age.totalDays + ") to int when calculating age! Only returning total days.\n\n" + e.Message);
                return age;
            }

            int currMonthI = start.date.month;
            int currYearI = start.date.year;
            int currMonthDays = GetTotalMonthDays(currMonthI, currYearI);
            while (age.days >= currMonthDays)
            {
                age.months++;
                currMonthI++;
                age.days -= currMonthDays;

                if (age.months > months.Length - 1)
                {
                    age.months = 0;
                    age.years++;
                    currYearI++;
                }
                if (currMonthI > months.Length - 1) currMonthI = 0;

                currMonthDays = GetTotalMonthDays(currMonthI, currYearI);
            }

            return age;
        }

        /// <summary>
        /// Find the DNCDatestamp that is the given days in either the future or past from now.
        /// </summary>
        /// <param name="days">The number of days from now.</param>
        /// <returns>The DNCDatestamp found.</returns>
        public DNCDatestamp FindDate(int days)
        {
            return FindDate(NowDatestamp, days);
        }
        /// <summary>
        /// Find the DNCDatestamp that is the given days in either the future or past from the given start.
        /// </summary>
        /// <param name="days">The number of days from start.</param>
        /// <returns>The DNCDatestamp found.</returns>
        public DNCDatestamp FindDate(DNCDatestamp start, int days)
        {
            if (days > 0) return FindFutureDate(start, days);
            else if (days < 0) return FindPastDate(start, Mathf.Abs(days));
            else return start;
        }

        /// <summary>
        /// Adds the given days to the time engine's managed time.
        /// </summary>
        /// <param name="days">The number of days to add.</param>
        public void AddDays(int days)
        {
            timeEngine.AddTime(0, 0, 0, days);
        }

        /// <summary>
        /// Is the given date in the future from now?
        /// </summary>
        /// <param name="date">The date to check.</param>
        /// <returns>True if it is in the future.</returns>
        public bool IsFuture(DNCDate date)
        {
            return IsFuture(NowDate, date);
        }
        /// <summary>
        /// Is the given date in the future from the given starting date?
        /// </summary>
        /// <param name="startDate">The starting date.</param>
        /// <param name="endDate">The date to check if is in the future from the starting date.</param>
        /// <returns>True if it is in the future.</returns>
        public bool IsFuture(DNCDate startDate, DNCDate endDate)
        {
            if (startDate != null && endDate != null)
            {
                if (endDate.year > startDate.year) return true;
                else if (endDate.year == startDate.year)
                {
                    if (endDate.month > startDate.month) return true;
                    else if (endDate.month == startDate.month)
                    {
                        if (endDate.day > startDate.day) return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Pauses the TimeEngine's time.
        /// </summary>
        public void PauseTime()
        {
            timeEngine.Pause();
        }
        /// <summary>
        /// Resumes the TimeEngine's time.
        /// </summary>
        public void ResumeTime()
        {
            timeEngine.Resume();
        }
        #endregion

        #region Private
        /// <summary>
        /// Find a DNCDatestamp that is in the future from the given start by a number of days.
        /// </summary>
        /// <param name="start">The starting date.</param>
        /// <param name="daysForward">The number of days to go in the future.</param>
        /// <returns>The DNCDatestamp found.</returns>
        private DNCDatestamp FindFutureDate(DNCDatestamp start, int daysForward)
        {
            if (daysForward <= 0) return start;
            else
            {
                DNCDatestamp end = start;
                int currMonthDays = GetTotalMonthDays(start.date.month);

                int d = daysForward;
                while (d > 0)
                {
                    end.date.day++;
                    d--;

                    while (end.date.day >= currMonthDays)
                    {
                        end.date.month++;
                        end.date.day %= currMonthDays;

                        if (end.date.month > months.Length - 1)
                        {
                            end.date.year += end.date.month / months.Length;
                            end.date.month %= months.Length;
                        }

                        currMonthDays = GetTotalMonthDays(end.date.month, end.date.year);
                    }
                }

                if (end.date.month > months.Length - 1)
                {
                    end.date.year += end.date.month / months.Length;
                    end.date.month %= months.Length;
                }

                end.season = CheckSeason(end.date.month, end.date.day);
                end.isLeapYear = CheckLeapYear(end.date.year);
                end.totalDays += (ulong)daysForward;

                return end;
            }
        }
        /// <summary>
        /// Find a DNCDatestamp that is in the past from the given start by a number of days.
        /// </summary>
        /// <param name="start">The starting date.</param>
        /// <param name="daysBack">The number of days to go in the past.</param>
        /// <returns>The DNCDatestamp found.</returns>
        private DNCDatestamp FindPastDate(DNCDatestamp start, int daysBack)
        {
            if (daysBack <= 0 || start.totalDays < (ulong)daysBack) return start;
            else if (start.totalDays == (ulong)daysBack) return worldStart;
            else
            {
                DNCDatestamp end = start;

                int d = daysBack;
                while (d > 0)
                {
                    end.date.day--;
                    d--;

                    while (end.date.day < 0)
                    {
                        end.date.month--;

                        if (end.date.month < 0)
                        {
                            end.date.month = months.Length - 1;
                            end.date.year--;
                        }

                        end.date.day = GetTotalMonthDays(end.date.month, end.date.year) - 1;
                    }
                }

                end.season = CheckSeason(end.date.month, end.date.day);
                end.isLeapYear = CheckLeapYear(end.date.year);
                end.totalDays -= (ulong)daysBack;

                return end;
            }
        }

        /// <summary>
        /// Update the current date.
        /// </summary>
        /// <param name="daysPassed">The days that have passed.</param>
        private void UpdateDate(int daysPassed)
        {
            bool dateChanged = false;
            DNCDatestamp newDate = FindDate(NowDatestamp, daysPassed);
            currDay = newDate.date.day;
            if (currMonth != newDate.date.month)
            {
                currMonth = newDate.date.month;
                OnMonthChanged?.Invoke(currMonth);
            }
            if (currYear != newDate.date.year)
            {
                currYear = newDate.date.year;
                OnYearChanged?.Invoke(currYear);
            }
            totalDays = newDate.totalDays;
            if (currSeason != newDate.season)
            {
                currSeason = newDate.season;
                OnSeasonChanged?.Invoke(currSeason);
            }

            if (dateChanged) OnDateChanged?.Invoke(NowDatestamp);
        }

        /// <summary>
        /// Initial setup.
        /// </summary>
        private void Setup()
        {
            if (timeEngine == null) Debug.LogError("No 'timeEngine' assigned!");
            if (months.Length == 0) Debug.LogError("No 'months' assigned!");
            if (seasons.Length == 0) Debug.LogError("No 'seasons' assigned!");

            timeEngine.OnDaysComplete += UpdateDate;
        }
        #endregion
        #endregion

        #region UnityMethods
        private void Awake()
        {
            Setup();
        }
        #endregion
    }

    /// <summary>
    /// A month.
    /// </summary>
    [Serializable]
    public class Month
    {
        [Tooltip("The name.")]
        public string name = "";
        [Tooltip("The total days in the month.")]
        public int days = 0;
        [Tooltip("The amount to adjust the days by during a leap year.")]
        public int leapYearDayAdjustment = 0;

        public Month()
        {

        }
        public Month(string n, int d)
        {
            name = n;
            days = d;
        }
        public Month(string n, int d, int lyd)
        {
            name = n;
            days = d;
            leapYearDayAdjustment = lyd;
        }

        /// <summary>
        /// Returns the month in the format: Name Days LeapYearAdjustment.
        /// </summary>
        /// <returns>The month in the format: Name Days LeapYearAdjustment.</returns>
        public override string ToString()
        {
            return string.Format("{0} {1} {2}", name, days, leapYearDayAdjustment);
        }
    }
    /// <summary>
    /// A season.
    /// </summary>
    [Serializable]
    public class Season
    {
        [Tooltip("The name.")]
        public string name = "";
        [Tooltip("The starting date for the season.")]
        public DNCDate startDate = new DNCDate();

        public Season()
        {

        }
        public Season(string n, DNCDate sd)
        {
            name = n;
            startDate = sd;
        }
        public Season(string n, int d, int m)
        {
            name = n;
            startDate = new DNCDate(d, m, 0);
        }

        /// <summary>
        /// Returns the season in the format: Name StartDate.
        /// </summary>
        /// <returns>The season in the format: Name StartDate.</returns>
        public override string ToString()
        {
            return string.Format("{0} {1}", name, startDate.ToString());
        }
    }

    /// <summary>
    /// A helper class that stores a date.
    /// </summary>
    [Serializable]
    public class DNCDate
    {
        [Tooltip("The day as a 0 base value.")]
        public int day = 0;
        [Tooltip("The month as a 0 base value.")]
        public int month = 0;
        [Tooltip("The year.")]
        public int year = 0;

        public DNCDate()
        {

        }
        public DNCDate(int d, int m, int y)
        {
            day = d;
            month = m;
            year = y;
        }

        /// <summary>
        /// Returns the date in the format: M/D/Y.
        /// </summary>
        /// <returns>The date in the format: M/D/Y</returns>
        public override string ToString()
        {
            return string.Format("{0}/{1}/{2}", day, month, year);
        }
    }
    /// <summary>
    /// The timestamp of a date.
    /// </summary>
    public class DNCDatestamp
    {
        public DNCDate date = new DNCDate();
        public int season = 0;
        public bool isLeapYear = false;
        public ulong totalDays = 0;

        public DNCDatestamp()
        {

        }
        public DNCDatestamp(int d, int m, int y, int s, bool ly, ulong td)
        {
            date = new DNCDate(d, m, y);
            season = s;
            isLeapYear = ly;
            totalDays = td;
        }
        public DNCDatestamp(DNCDate d, int s, bool ly, ulong td)
        {
            date = d;
            season = s;
            isLeapYear = ly;
            totalDays = td;
        }

        /// <summary>
        /// Returns the date in the format: M/D/Y Season LeapYear TotalDays.
        /// </summary>
        /// <returns>The date in the format: M/D/Y Season LeapYear TotalDays</returns>
        public override string ToString()
        {
            return string.Format("{0} {1} {2} {3}", date.ToString(), season, isLeapYear, totalDays);
        }
    }
    /// <summary>
    /// A helper class for storing values for an age.
    /// </summary>
    public class DNCAge
    {
        public int days = 0;
        public int months = 0;
        public int years = 0;
        public ulong totalDays = 0;

        public DNCAge()
        {

        }
        public DNCAge(int d, int m, int y, ulong td)
        {
            days = d;
            months = m;
            years = y;
            totalDays = td;
        }

        /// <summary>
        /// Returns the age in the format: D, M. Y, TotalDays.
        /// </summary>
        /// <returns>The age in the format: D, M. Y, TotalDays</returns>
        public override string ToString()
        {
            return string.Format("{0}, {1}, {2}, {3}", days, months, years, totalDays);
        }
    }
}