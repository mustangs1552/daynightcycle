﻿using System;
using UnityEngine;

namespace MattRGeorge.DayNightCycle.Engines
{
    /// <summary>
    /// Manages the seconds, minutes, and hours of in-game time.
    /// </summary>
    public class TimeEngine : MonoBehaviour
    {
        #region Variables
        #region Public
        [Tooltip("The number of seconds that are added whenever the time is updated.")]
        [SerializeField] private int clockSpeed = 1;
        [Tooltip("The real-time seconds interval that the time is updated.")]
        [SerializeField] private int interval = 1;
        [Tooltip("The starting time.")]
        [SerializeField] private DNCTime startTime = new DNCTime(0, 0, 0);
        [Tooltip("The number of hours in one day.")]
        [SerializeField] private int hrsInDay = 24;

        /// <summary>
        /// Returns a DNCTime with the current time.
        /// </summary>
        public DNCTime NowTime
        {
            get
            {
                return new DNCTime(currSec, currMin, currHr);
            }
        }
        /// <summary>
        /// Returns a DNCTimestamp with the current time.
        /// </summary>
        public DNCTimestamp NowTimestamp
        {
            get
            {
                return new DNCTimestamp(NowTime, totalSecs, totalTimeUpdates, DayProgressPerc);
            }
        }

        /// <summary>
        /// Called when time is updated.
        /// Param 1 (int): Seconds passed.
        /// Param 2 (DNCTimestamp): Current time.
        /// </summary>
        public Action<int, DNCTimestamp> OnTimeUpdate { get; set; }
        /// <summary>
        /// Called when at least a day has passed.
        /// Passes an int with the number of days passed.
        /// </summary>
        public Action<int> OnDaysComplete { get; set; }

        /// <summary>
        /// The progress in the day as a 0-1 value.
        /// </summary>
        public float DayProgressPerc
        {
            get
            {
                int secsCurrDay = currSec + (currMin * 60) + (currHr * 60 * 60);
                return (secsCurrDay != 0) ? (float)(secsCurrDay) / (float)(hrsInDay * 60 * 60) : 0;
            }
        }
        /// <summary>
        /// The progress in the day as a decimal.
        /// </summary>
        public float TimeToDec
        {
            get
            {
                return (float)currHr + ((float)currMin / 60);
            }
        }

        /// <summary>
        /// The current number of seconds that are added each time update.
        /// </summary>
        public int CurrClockSpeed
        {
            get
            {
                return currClockSpeed;
            }
            set
            {
                if (value != 0) currClockSpeed = value;
            }
        }
        /// <summary>
        /// The current interval that time is being updated by.
        /// </summary>
        public int CurrInterval
        {
            get
            {
                return currInterval;
            }
            set
            {
                if (value > 0)
                {
                    currInterval = value;
                    Resume();
                }
            }
        }
        /// <summary>
        /// The current seconds.
        /// </summary>
        public int CurrSec
        {
            get
            {
                return currSec;
            }
        }
        /// <summary>
        /// The current minutes.
        /// </summary>
        public int CurrMin
        {
            get
            {
                return currMin;
            }
        }
        /// <summary>
        /// The current hours.
        /// </summary>
        public int CurrHr
        {
            get
            {
                return currHr;
            }
        }
        /// <summary>
        /// The total seconds that has passed since start.
        /// </summary>
        public ulong TotalSecs
        {
            get
            {
                return totalSecs;
            }
        }
        /// <summary>
        /// The total number of updates that has passed since start.
        /// </summary>
        public ulong TotalTimeUpdates
        {
            get
            {
                return totalTimeUpdates;
            }
        }
        #endregion

        #region Private
        private int currClockSpeed = 0;
        private int currInterval = 0;
        private int currSec = 0;
        private int currMin = 0;
        private int currHr = 0;
        private ulong totalSecs = 0;
        private ulong totalTimeUpdates = 0;
        #endregion
        #endregion

        #region Methods
        #region Public
        /// <summary>
        /// Starts the time.
        /// This must be called before you can call some other methods.
        /// </summary>
        public void StartTime()
        {
            SetTime(startTime);

            totalSecs = 0;
            totalTimeUpdates = 0;

            Resume();
        }

        /// <summary>
        /// Pauses the time.
        /// </summary>
        public void Pause()
        {
            if (IsInvoking("UpdateTime")) CancelInvoke("UpdateTime");
        }
        /// <summary>
        /// Resumes time.
        /// Time must have been started first.
        /// </summary>
        public void Resume()
        {
            if (IsInvoking("UpdateTime")) CancelInvoke("UpdateTime");
            currInterval = (currInterval <= 0) ? 1 : currInterval;
            InvokeRepeating("UpdateTime", currInterval, currInterval);
        }

        /// <summary>
        /// Adds the the given values to the current time.
        /// Time must have been started first.
        /// </summary>
        /// <param name="secs">Number of seconds to add.</param>
        /// <param name="mins">Number of minutes to add.</param>
        /// <param name="hrs">Number of hours to add.</param>
        /// <param name="days">Number of days worth of hours to add.</param>
        public void AddTime(int secs, int mins, int hrs, int days = 0)
        {
            currSec += secs;
            currMin += mins + currSec / 60;
            hrs += days * hrsInDay;
            currHr += hrs + currMin / 60;

            if (currHr >= hrsInDay) OnDaysComplete?.Invoke(currHr / hrsInDay);

            currSec %= 60;
            currMin %= 60;
            currHr %= hrsInDay;

            totalSecs += (ulong)(secs + (mins * 60) + (hrs * 60 * 60));
            if (secs > 0 || mins > 0 || hrs > 0 || days > 0) OnTimeUpdate?.Invoke(secs + (mins * 60) + (hrs * 60 * 60), NowTimestamp);
        }

        /// <summary>
        /// Sets the time to the given time.
        /// </summary>
        /// <param name="time">The desired time.</param>
        public void SetTime(DNCTimestamp time)
        {
            SetTime(time.time.sec, time.time.min, time.time.hr);
        }
        /// <summary>
        /// Sets the time to the given time.
        /// </summary>
        /// <param name="time">The desired time.</param>
        public void SetTime(DNCTime time)
        {
            SetTime(time.sec, time.min, time.hr);
        }
        /// <summary>
        /// Sets the time to the given time.
        /// </summary>
        /// <param name="secs">The desired seconds.</param>
        /// <param name="mins">The desired minutes.</param>
        /// <param name="hrs">The desired hours.</param>
        public void SetTime(int secs, int mins, int hrs)
        {
            bool timeChanged = secs != currSec || mins != currMin || hrs != currHr;

            currSec = secs % 60;
            currMin = mins % 60;
            currHr = hrs % hrsInDay;

            if (timeChanged) OnTimeUpdate?.Invoke(0, NowTimestamp);
        }
        #endregion

        #region Private
        /// <summary>
        /// Updates the time by the current clock speed.
        /// </summary>
        private void UpdateTime()
        {
            AddTime(currClockSpeed, 0, 0);

            totalTimeUpdates++;
        }

        /// <summary>
        /// Initial setup.
        /// </summary>
        private void Setup()
        {
            currClockSpeed = clockSpeed;
            currInterval = (interval <= 0) ? 1 : interval;
        }
        #endregion
        #endregion

        #region UnityMethods
        private void Awake()
        {
            Setup();
        }

        private void OnDisable()
        {
            Pause();
        }
        private void OnEnable()
        {
            Resume();
        }
        #endregion
    }

    /// <summary>
    /// A helper class that stores a time.
    /// </summary>
    [Serializable]
    public class DNCTime
    {
        public int sec = 0;
        public int min = 0;
        public int hr = 0;

        public DNCTime()
        {

        }
        public DNCTime(int s, int m, int h)
        {
            sec = s;
            min = m;
            hr = h;
        }

        /// <summary>
        /// Returns the time in the format: H:M:S.
        /// </summary>
        /// <returns>The time in the format: H:M:S.</returns>
        public override string ToString()
        {
            return string.Format("{0}:{1}:{2}", hr, min, sec);
        }
    }

    /// <summary>
    /// A timestamp of the current time.
    /// These values can't be changed once they are set.
    /// </summary>
    public class DNCTimestamp
    {
        public DNCTime time = new DNCTime();
        public ulong totalSecs = 0;
        public ulong totalTimeUpdates = 0;
        public float dayProgress = 0;

        public DNCTimestamp()
        {

        }
        public DNCTimestamp(int sec, int min, int hr, ulong ts, ulong ttu, float dp)
        {
            time = new DNCTime(sec, min, hr);
            totalSecs = ts;
            totalTimeUpdates = ttu;
            dayProgress = dp;
        }
        public DNCTimestamp(DNCTime t, ulong ts, ulong ttu, float dp)
        {
            time = t;
            totalSecs = ts;
            totalTimeUpdates = ttu;
            dayProgress = dp;
        }

        /// <summary>
        /// Returns the time in the format: H:M:S TotalSec:TotalTimeUpdates DayProgress.
        /// </summary>
        /// <returns>The time in the format: H:M:S TotalSec:TotalTimeUpdates DayProgress.</returns>
        public override string ToString()
        {
            return string.Format("{0} {1}:{2} {3}", time.ToString(), totalSecs, totalTimeUpdates, dayProgress);
        }
    }
}
